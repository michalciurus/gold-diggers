using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ViewController : MonoBehaviour 
{
	public List<PanelRecord> panels;
		
	public Transform 	activePanelPosition;
	public Transform 	nextPanelPosition;
	public Transform 	closedPanelPosition;
	
	private Dictionary<string, PanelRecord> _panelRegister;
	private string 		_currentPanelName;
	private string		_nextPanel;
	private string		_prevPanel;
	
	private bool		_isTweenCompleted  = true;
	
	private void Start()
	{
		Application.targetFrameRate = 60;
		init();
	}
	
	public void init()
	{
		CreatePanelRegister();
		_currentPanelName = "";
		ShowPanel("MainMenu");
	}
	
	private void CreatePanelRegister()
	{
		_panelRegister = new Dictionary<string, PanelRecord>();
		
		for(int i = 0; i < panels.Count; i++)
		{
			_panelRegister.Add(panels[i].panelName, panels[i]);
		}
	}
	
	private PanelRecord GetPanel(string panelName)
	{
		if(_panelRegister != null)
		{
			if(_panelRegister.ContainsKey(panelName))
			{
				return _panelRegister[panelName];
			}
			else
			{
				Debug.LogError(panelName + " Requested panel is not registred in panel register");
				return null;
			}
		}
		else
		{
			Debug.LogError("Panel register is not initialized");
			return null;
		}
	}
	
	private void ShowPanel(string panelName)
	{
		if(_panelRegister.ContainsKey(panelName))
		{
			if(_isTweenCompleted && panelName != _currentPanelName)
			{
				if ( !_currentPanelName.Equals(string.Empty))
				GetPanel(_currentPanelName).panel.SendMessage("release",SendMessageOptions.DontRequireReceiver);
				GetPanel(panelName).panel.SendMessage(CTools.INIT_MESSAGE,SendMessageOptions.DontRequireReceiver);
				_nextPanel = panelName;
				SwitchPanels();
			}
		}
		else
		{
			Debug.LogError(panelName + " ShowPanel: Requested panel is not registred in panel register");
		}
	}
	
	private void SwitchPanels()
	{
		//_isTweenCompleted = false;
		
		if(_nextPanel != _prevPanel)
		{
			if(! string.IsNullOrEmpty(_currentPanelName))
			{
				GetPanel(_currentPanelName).SetTween(activePanelPosition, closedPanelPosition);
				GetPanel(_currentPanelName).PlayTween();
			}
			
			GetPanel(_nextPanel).SetTween(nextPanelPosition, activePanelPosition);
			GetPanel(_nextPanel).PlayTween();
			
			_prevPanel 			= _currentPanelName;
			_currentPanelName 	= _nextPanel;
			_nextPanel 			= "";
		}
		else
		{
			if(! string.IsNullOrEmpty(_currentPanelName))
			{
				GetPanel(_currentPanelName).SetTween(activePanelPosition, nextPanelPosition);
				GetPanel(_currentPanelName).PlayTween();
			}
			
			GetPanel(_nextPanel).SetTween(closedPanelPosition, activePanelPosition);
			GetPanel(_nextPanel).PlayTween();
			
			_prevPanel 			= _currentPanelName;
			_currentPanelName 	= _nextPanel;
			_nextPanel 			= "";
		}
	}
	
	private void TweenFinished()
	{
		_isTweenCompleted = true;
	}
}
	