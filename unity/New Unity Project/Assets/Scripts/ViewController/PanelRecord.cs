using UnityEngine;

[System.Serializable]
public class PanelRecord
{
	public string 			panelName;
	public GameObject 		panel;
	private bool			childrenEnabled;
	
	private TweenTransform tween;
	
	public PanelRecord()
	{
		childrenEnabled = true;	
	}
	
	public void AddTween()
	{
		if(!isTweenAdded())
		{
			tween 			= panel.gameObject.AddComponent<TweenTransform>();
			tween.style 	= UITweener.Style.Once;
			tween.duration	= 0.5f;
			tween.method	= UITweener.Method.EaseInOut;
//			tween.eventReceiver = GameObject.FindGameObjectWithTag("ViewControler");
			tween.callWhenFinished = "TweenFinished";
		}
		else
		{
			Debug.LogError(panelName + " - AddTween: Tween is already added");
		}
	}
	
	public void SetTween(Transform startPos, Transform targetPos)
	{
		if(isTweenAdded())
		{
			tween.from 	= startPos;
			tween.to	= targetPos;
		}
		else
		{
			//Debug.LogError(panelName+" - SetTween: Tween is nod added");
			AddTween();
			tween.from 	= startPos;
			tween.to	= targetPos;
		}
	}
	
	public void PlayTween()
	{
		if(isTweenAdded())
		{
			tween.Reset();
			tween.Play(true);
		}
		else
		{
			//Debug.LogError(panelName+" - PlayTween: Tween is nod added");
			AddTween();
			tween.Reset();
			tween.Play(true);
		}
	}

	public void switchDisableChildren(bool IsEnabled)
	{
		this.panel.gameObject.SetActive(IsEnabled);
	}
	
	private bool isTweenAdded()
	{
		TweenTransform temp = panel.gameObject.GetComponent<TweenTransform>();
		
		if(temp != null)
		{
			if(tween != null)
			{
				tween = temp;
			}
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
}