﻿using UnityEngine;
using System.Collections;
using System;

public class ShopView : MonoBehaviour {
	
	private UILabel _lifeLabel;
	private UILabel _planeLabel;
	private UILabel _inviLabel;
	private UILabel _shopCoinsLabel;
	

	// Use this for initialization
	void Start () {
		
		_lifeLabel = GameObject.Find ("LifeNumber").GetComponent<UILabel>();
		_planeLabel = GameObject.Find ("PlaneNumber").GetComponent<UILabel>();
		_inviLabel = GameObject.Find ("InviNumber").GetComponent<UILabel>();
		_shopCoinsLabel = GameObject.Find ("ShopCoinsLabel").GetComponent<UILabel>();
		
		updateLabels();
		setPricesInfo();
	}
	
	
	public void init()
	{
		updateLabels();
	}
	// Update is called once per frame
	void Update () {
		
	}
	
	private void setPricesInfo()
	{
		GameObject.Find("InviPrice").GetComponent<UILabel>().text = CTools.SHOP_INVI_PRICE.ToString() + "c";
		GameObject.Find("PlanePrice").GetComponent<UILabel>().text = CTools.SHOP_PLANE_PRICE.ToString() + "c";
		GameObject.Find("LifePrice").GetComponent<UILabel>().text = CTools.SHOP_LIFE_PRICE.ToString() + "c";
	}
	
	public void updateLabels()
	{
		_shopCoinsLabel.text = XMLGameData.XMLGameDataInstance._numberOfCoins;
 		_lifeLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerLives;
		_planeLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerPlanes;
		_inviLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerInvi;
	}
	
	public void release()
	{
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("");
	}
	
	
}
