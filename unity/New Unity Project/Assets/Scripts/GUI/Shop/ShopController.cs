﻿using UnityEngine;
using System.Collections;
using System;

public class ShopController : MonoBehaviour {
	
	private ShopView _view;
	
	// Use this for initialization
	void Start () {
	_view = this.GetComponent<ShopView>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void tryBuyLifePowerup()
	{
		int numberOfCoins = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins);
		
		if (numberOfCoins >= CTools.SHOP_LIFE_PRICE)
		{
			int numberOfPower = Convert.ToInt32( XMLGameData.XMLGameDataInstance._numberOfPowerLives);
			numberOfCoins -= CTools.SHOP_LIFE_PRICE;
			
			XMLGameData.XMLGameDataInstance._numberOfCoins = numberOfCoins.ToString();
			XMLGameData.XMLGameDataInstance._numberOfPowerLives = (++numberOfPower).ToString();
			
			XMLGameData.XMLGameDataInstance.Save();
		}
		else
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("NOT ENOUGH COINS");
		_view.updateLabels();
	}
	
	public void tryBuyPlanePowerup()
	{
		
		int numberOfCoins = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins);
		
		if (numberOfCoins >= CTools.SHOP_PLANE_PRICE)
		{
			int numberOfPower = Convert.ToInt32( XMLGameData.XMLGameDataInstance._numberOfPowerPlanes);
			numberOfCoins -= CTools.SHOP_PLANE_PRICE;
				
			XMLGameData.XMLGameDataInstance._numberOfCoins = numberOfCoins.ToString();
			XMLGameData.XMLGameDataInstance._numberOfPowerPlanes = (++numberOfPower).ToString();
			
			XMLGameData.XMLGameDataInstance.Save();
		}
		else
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("NOT ENOUGH COINS");
		_view.updateLabels();
	}
	
	public void tryBuyInviPowerup()
	{
		int numberOfCoins = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins);
		
		if (numberOfCoins >= CTools.SHOP_INVI_PRICE)
		{
			int numberOfPower = Convert.ToInt32( XMLGameData.XMLGameDataInstance._numberOfPowerInvi);
			numberOfCoins -= CTools.SHOP_INVI_PRICE;
				
			XMLGameData.XMLGameDataInstance._numberOfCoins = numberOfCoins.ToString();
			XMLGameData.XMLGameDataInstance._numberOfPowerInvi = (++numberOfPower).ToString();
			
			XMLGameData.XMLGameDataInstance.Save();
		}
		else
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("NOT ENOUGH COINS");
		_view.updateLabels();
}
}
