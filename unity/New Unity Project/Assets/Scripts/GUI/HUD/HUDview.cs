﻿using UnityEngine;
using System.Collections;

public class HUDview : MonoBehaviour {
	
	private UILabel _messageLabel;
	private UIImageButton _imageButton;

	// Use this for initialization
	void Start () {
	_messageLabel = GameObject.Find ("MessageLabel").GetComponent<UILabel>();
	_imageButton = GameObject.Find("soundButton").GetComponent<UIImageButton>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void showMessage(string message)
	{
		_messageLabel.text = message;
	}
	
	public void switchButton()
	{
		string normal =  _imageButton.normalSprite;
		_imageButton.normalSprite = _imageButton.pressedSprite;
		_imageButton.pressedSprite = normal;
	}
}
