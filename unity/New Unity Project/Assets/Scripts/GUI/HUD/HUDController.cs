﻿using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour {
	
	private CTimer _timer;
	private HUDview _view;

	// Use this for initialization
	void Start () {
	_timer = new CTimer();
	_view = this.GetComponent<HUDview>() as HUDview;
	}
	
	public void showMessage(string message)
	{
		
		_view.showMessage(message);
		_timer.setTimedCallback(deleteMessage, CTools.MESSAGE_TIME);
	}
	
	public void deleteMessage()
	{
	 	_view.showMessage(string.Empty);	
	}
	
	// Update is called once per frame
	void Update () {
		_timer.UpdateTimer();
	}
	
	public void muteSoundsSwitch()
	{
		_view.switchButton();
		CSoundManager.soundManagerInstance.muteSounds();
	}
}
