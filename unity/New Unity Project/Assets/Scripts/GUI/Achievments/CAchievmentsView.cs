﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CAchievmentsView : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void init()
	{
		loadAchievments();
	}
	
	void loadAchievments()
	{
	
		List<SAchievment> toLoad = CAchievmentsManager.managerInstance.getAchievmentList();
		
		for( int i = 0; i < toLoad.Count ;i++ )
		{
			GameObject draggablePanel = GameObject.Find ("dragPanel");
			GameObject achievment = GameObject.Instantiate(CGraphicManager.GameLoaderInstance.getPrefabFromCache(CTools.ACHIEVMENT_PREFAB_LOCATION)) as GameObject;
			UILabel label = achievment.GetComponentInChildren<UILabel>();
			UISprite sprite = achievment.GetComponentInChildren<UISprite>();
			achievment.transform.parent = draggablePanel.transform;
			achievment.GetComponent<UIDragPanelContents>().draggablePanel = draggablePanel.GetComponent<UIDraggablePanel>();
			achievment.transform.localPosition = new Vector3(0, Screen.height/2 - 50 - 250*i,0);
			achievment.transform.localScale = Vector3.one;
			label.text = toLoad[i].name;
			
			if (toLoad[i].isDone)
			sprite.color = Color.yellow;
			
		
		}
		
	}
}
