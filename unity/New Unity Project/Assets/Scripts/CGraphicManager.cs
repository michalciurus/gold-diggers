// @file CGameLoader.cs
// loads prefabs and textures from texture atlas and caches them

using UnityEngine;
using System;
using SimpleJSON;
using System.Collections.Generic;




//Singleton that manages atlas extracting and handing out textures from given atlases
public class CGraphicManager
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	
// Structure that holds data from .json 
private struct STextureData
{ 
	string textureName;
	public	float x ;
	public float y;
	public	float w ;
	public	float h ;
}

	
//Structure that holds data loaded and parsed from Resources: texture atlas and texture coordinates
private struct TextureAtlasData
{
	public Texture2D atlasTexture;
	public JSONNode atlasJsonNode;
	
	public TextureAtlasData (Texture2D pAtlasTexture, JSONNode pAtlasJsonNode)
	{
		this.atlasTexture = pAtlasTexture;
		this.atlasJsonNode = pAtlasJsonNode;
	}
	
}
	

	private static CGraphicManager _GameLoaderInstance;
	private Dictionary<string, TextureAtlasData> _atlasJsonDictionary;
	private Dictionary<string, GameObject> _prefabCache;
	private Dictionary<string, Texture2D> _textureCache;

	private CGraphicManager ()
	{
		_atlasJsonDictionary = new Dictionary<string, TextureAtlasData> ();
		_prefabCache = new Dictionary<string, GameObject>();
		_textureCache = new Dictionary<string, Texture2D>();
	}
	
//------------------------------------------------------
// Public Variables
//------------------------------------------------------
	
	//Creating a singleton
	public static CGraphicManager GameLoaderInstance {
		get {
			if (_GameLoaderInstance == null) {
				_GameLoaderInstance = new CGraphicManager ();	
			}
			return _GameLoaderInstance;
		}
			
	}
	
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	//Loading atlas files + parsing
	// Parameters:
	// pJsonName - name of the texture asset 
	// pTextureName - name of the texture read from .json
	public void  loadAtlas (string pJsonName, string pTextureName)
	{
		TextAsset latlasJsonFile = Resources.Load ("Atlas/" + pJsonName) as TextAsset;
		Texture2D ltextureAtlas = Resources.Load ("Atlas/" + pTextureName) as Texture2D;		
		//Checking if the files were loaded properly
		if (latlasJsonFile != null && ltextureAtlas != null) {
		//Parsing the .json data
			JSONNode parsedAtlasArray = JSON.Parse (latlasJsonFile.ToString ());
			_atlasJsonDictionary.Add (pJsonName, new TextureAtlasData (ltextureAtlas, parsedAtlasArray));
		} else {
			// error handle
			Debug.Log ("couldn't load");
		}
	}
	
	//Method returns a texture loaded from an atlas
	//Parameters:
	// pAtlasName - name of the atlas in which the texture is located
	// pTextureName - name of the texture to load
	public Texture2D cacheTextureFromAtlas (string pAtlasName, string pTextureName)
	{	
		
		int textureX;
		int textureY;
		int textureW;
		int textureH;
		
		if (_atlasJsonDictionary.ContainsKey (pAtlasName)) {
			TextureAtlasData currentAtlasData = _atlasJsonDictionary [pAtlasName];
			// Reading texture data from parsed .json
			JSONNode currentNode = currentAtlasData.atlasJsonNode;
			textureX = currentNode [0] [pTextureName] ["frame"] ["x"].AsInt;
			textureY = currentNode [0] [pTextureName] ["frame"] ["y"].AsInt;
			textureW = currentNode [0] [pTextureName] ["frame"] ["w"].AsInt;
			textureH = currentNode [0] [pTextureName] ["frame"] ["h"].AsInt;
			// Converting coordination system to Unity3d
			int convertCoord = currentAtlasData.atlasTexture.height - textureY - textureH;
			// Reading pixels from atlas
			Color[] texturePixels = currentAtlasData.atlasTexture.GetPixels (textureX, convertCoord, textureW, textureH);
		
			Texture2D textureToReturn = new Texture2D (textureW, textureH);
			textureToReturn.SetPixels (texturePixels);
			textureToReturn.Apply ();
		
			this._textureCache.Add(pTextureName,textureToReturn);
			return textureToReturn;
			
		} else {
			//error
			return null;
		}
			
	}
	
	// add a prefab to cache
	public GameObject cachePrefab(string pPrefabLocation)
	{
		GameObject objectToReturn = Resources.Load(pPrefabLocation) as GameObject;
		this._prefabCache.Add(pPrefabLocation,objectToReturn);
		return objectToReturn;
	}
	
	// add a texture to cache
	public Texture2D cacheTexture(string pTextureLocation)
	{
		Texture2D textureToReturn = Resources.Load(pTextureLocation) as Texture2D;
		this._textureCache.Add (pTextureLocation, textureToReturn);	
		return textureToReturn;
	}
	
	public GameObject getPrefabFromCache(string pPrefabLocation)
	{
		if (this._prefabCache.ContainsKey(pPrefabLocation))
		return this._prefabCache[pPrefabLocation];
		else
		{
			return null;
		}
	}
	
	public Texture2D getTextureFromCache(string pTextureName)
	{
		if ( this._textureCache.ContainsKey(pTextureName) )
		return this._textureCache[pTextureName];
		else
			return null;
	}
	

}
