using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;


	[XmlRoot("GameData")]
	public class XMLGameData
	{

		private static XMLGameData _XMLGameDataInstance;
		[XmlAttribute("coins")]
		public string _numberOfCoins { get; set; }
		[XmlAttribute("powerLives")]
		public string _numberOfPowerLives { get; set;  }
		[XmlAttribute("powerPlanes")]
		public string _numberOfPowerPlanes {  get; set;  }
		[XmlAttribute("powerInvi")]
		public string _numberOfPowerInvi {  get; set;  }
		[XmlArray("Achievments")]
		[XmlArrayItem("Achievment")]
		public List<SAchievment> _achievments {get; set;}	
	
		public static XMLGameData XMLGameDataInstance
	{
		get {
				if ( _XMLGameDataInstance == null)
				{
					_XMLGameDataInstance = new XMLGameData();	
				}
				return _XMLGameDataInstance;
	}
	}
		
		private XMLGameData ()
		{

		}
	
	// serializes this class to xml
	public void Save()
 	{
 		var serializer = new XmlSerializer(typeof(XMLGameData));
 		using(var stream = new FileStream(Path.Combine(Application.dataPath, CTools.XML_DATA_NAME), FileMode.Create))
 		{
 			serializer.Serialize(stream, this);
 		}
		
		CAchievmentsManager.managerInstance.checkAchievmentsConditions();
 	}
 	// serializes this class from xml
 	public void Load()
 	{
 		var serializer = new XmlSerializer(typeof(XMLGameData));
 		using(var stream = new FileStream(Path.Combine(Application.dataPath, CTools.XML_DATA_NAME), FileMode.Open))
 		{
 			_XMLGameDataInstance = serializer.Deserialize(stream) as XMLGameData;
 		}
 	}
	}


