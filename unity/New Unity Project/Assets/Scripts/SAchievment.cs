using System;
using System.Xml;
using System.Xml.Serialization;

	public class SAchievment
	{  
		public delegate bool _callbackMethodDel();
		[XmlIgnoreAttribute]
		public _callbackMethodDel _conditionCallback;
		public bool isDone;
		public string name;
	
		public SAchievment() { }
	
		public SAchievment(_callbackMethodDel callback, string pName)
		{
			_conditionCallback = callback;
			isDone = false;
			name = pName;
		
		}
	}


