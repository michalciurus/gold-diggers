// @file CTimer.cs
// a timer that calls a function given after the time has passed

using System;
using UnityEngine;
using System.Collections.Generic;

	public class CTimer
	{
		private List<timeEvent> _listOfEvents;	
	
	// holds an event to get called after given time
		private struct timeEvent
	{
		public delegate void _callbackMethodDel();
		public _callbackMethodDel _TimerCallback;
		public float _timeDue;
		
		// 
		// @param pCallbackMethod - method to be called
		// @param pHowMuchTime - after how much time
		public timeEvent(Action pCallbackMethod, float pHowMuchTime)
		{
			_timeDue = Time.time + 	pHowMuchTime;
			_TimerCallback = new _callbackMethodDel(pCallbackMethod);
			
		}
	}
	
		public CTimer ()
		{
			this._listOfEvents = new List<timeEvent>();
		}
	
		// 
		// @param pCallbackMethod - method to be called
		// @param pHowMuchTime - after how much time
		public void setTimedCallback(Action pMethodTocall, float howMuchTime)
	{
		timeEvent tEvent = new timeEvent(pMethodTocall, howMuchTime);
		this._listOfEvents.Add(tEvent);
	}
	
	// Timer needs to get updated
	public void UpdateTimer()
	{
		List<timeEvent> toClear = new List<timeEvent>();
		
		// Checks if any callback method is ready to get called
		foreach (timeEvent tEvent in _listOfEvents)
		{
			if ( tEvent._timeDue < Time.time )
			{
			tEvent._TimerCallback();	
			toClear.Add(tEvent);
			}
		}
		
		// Clearing the called events
		foreach(timeEvent eventToClear in toClear)
		{
		this._listOfEvents.Remove (eventToClear);	
		}
	}
	}


