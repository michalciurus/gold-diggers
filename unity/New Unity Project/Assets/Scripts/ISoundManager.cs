using System;


	public interface ISoundManager
{
	void playShootSound();
	void playMonsterSound();
	void playDamageSound();
	void playCoinSound();
	void muteSounds();
}

