/*achievment manager singleton*/

using System;
using System.Collections.Generic;
using UnityEngine;


	public class CAchievmentsManager
	{
	
		private static CAchievmentsManager _managerInstance;
		private List<SAchievment> _achievmentList;
		
		public static CAchievmentsManager managerInstance 
		{ get { if (_managerInstance == null)
					_managerInstance = new CAchievmentsManager();
		 	 return _managerInstance;
			}
		}
	
	
		private CAchievmentsManager ()
		{
		_achievmentList = new List<SAchievment>();
		addAchievments();
		}
		
		// to add an achievment the only thing needed is a function and a name for that achievment and that's it
		// it'll get automatically added to xml and to Achievment panel
		private void addAchievments()
		{
		_achievmentList.Add(new SAchievment(tenCoins, "Get 10 coins!"));
		_achievmentList.Add(new SAchievment(fiftyCoins, "Get 50 coins!"));
		_achievmentList.Add(new SAchievment(hundredCoins, "Get 100 coins!"));
		_achievmentList.Add(new SAchievment(tenLifes, "Get 10 lifes!"));
		}
	
	// Achievment functions
		public bool tenCoins()
		{
			return Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins) >= 10;
		}
	
		public bool fiftyCoins()
		{
			return Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins) >= 50;
		}
	
		public bool hundredCoins()
		{
			return Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins) >= 100;
		}
	
		public bool tenLifes()
		{
			return Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfPowerLives) >= 10;
		}
	//
		
	
		// updates with new achievments but doesnt overwrite the old
		// used for updating MXL for new achievments
		public void updateXML()
		{
			List<SAchievment> XMLlist = XMLGameData.XMLGameDataInstance._achievments;
			List<SAchievment> toAdd = new List<SAchievment>();
			bool exists = false;
			
			for (int i = 0 ; i < _achievmentList.Count; i++)
		{
			SAchievment achv = _achievmentList[i];
			
				foreach( SAchievment xmlAchv in XMLlist)
			{
				
					if ( achv.name.Equals(xmlAchv.name))
				{
					exists = true;
				}
			}
			if ( !exists )
				XMLlist.Add(achv);
		}
		
		loadFromXML();
		XMLGameData.XMLGameDataInstance._achievments = _achievmentList;
		XMLGameData.XMLGameDataInstance.Save();
	
		
		}
	
		// loads data from xml
		private void loadFromXML()
		{
		
			foreach(SAchievment xmlAchv in XMLGameData.XMLGameDataInstance._achievments)
		{	
			for ( int i = 0; i < _achievmentList.Count; i++)
			{
				if ( xmlAchv.name.Equals(_achievmentList[i].name))
				{
					_achievmentList[i].isDone = xmlAchv.isDone;
					
					continue;
				}
				
			}
			
			
		}
		
		
		}
	
		//updating achievments
		public void checkAchievmentsConditions()
		{
			bool changed = false;
		
			for ( int i = 0; i < _achievmentList.Count ; i++)
			if ( !_achievmentList[i].isDone && _achievmentList[i]._conditionCallback() )
		{
			changed = true;
				_achievmentList[i].isDone = true;
			(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage(_achievmentList[i].name +  " UNLOCKED");
			
		}
		if ( changed)
			XMLGameData.XMLGameDataInstance.Save();
				
		}
	
		public List<SAchievment> getAchievmentList()
	{
		return _achievmentList;	
	}
		
	
	
		
	
		
	
	}


