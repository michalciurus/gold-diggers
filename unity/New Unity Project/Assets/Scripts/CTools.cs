// @file Ctools.cs
// holds all game static data
using System;
using UnityEngine;

// CLASS TO HOLD ALL STATIC DATA NEEDED TO PLAY THE GAME
public static class CTools
{
	
	public static System.Random _rnd = new System.Random ();
	// how many frames is the next wagon delayed
	public const int WAGON_DELAY = 7;
	// how far are the wagons from each other
	public const float WAGON_OFFSET_POSITION = 0.117f;
	public const float SPEED_SLOW_DOWN = 0.2f;
	// Important: velocity during input
	public const int VELOCITY_MULTIPLIER = 150;
	// max angle during input
	public const int MAX_WAGON_ANGLE = 130;
	// decrease angle when idle
	public const int ANGLE_WAGON_DECREASE = 1;
	public const float BACKGROUND_SPEED = 0.4f;
	// how far are the thrusters
	public const float THRUSTERS_OFFSET = 0.039f;
	// how big in Y is the region of random events
	public const int RANDOM_REGION = 3;
	public const int COINS_CHANCE = 40;
	public const int FIRE_CHANCE = 60;
	public const float ELEMENTS_SPEED = 1.69f;
	public const int NUMBER_OF_WAGONS = 3;
	public const int RANDOM_FIRE_CHANCE = 60;
	public const int TIME_TO_RECOVER = 3;
	public const float BOUNCE_OFF_ROCK_OFFSET = 0.26f;
	public const int MONSTER_SPEED = 2;
	public const float COLOR_CHANGE_SPEED = 0.05f;
	public const int PLANE_CIRCLE_SPEED = 2;
	public const float PLANE_DISTANCE = 0.26f;
	public const float GUIDED_MISSILE_SPEED = 5;
	public const float MESSAGE_TIME = 2;
	public const float MISSILE_SPEED = 35;
	public const float SHOOT_DELAY = 0.5f;
	public const int SHOP_LIFE_PRICE = 20;
	public const int SHOP_PLANE_PRICE = 20;
	public const int SHOP_INVI_PRICE = 30;
	public const float PLANE_TIME = 10;
	public const float POWERUP_SIZE = 1;
	public const int FRAMERATE = 60;
	public const float PLANE_VIEW_OFFSET = 0.2f;
	public const int MAX_FIRE_PER_REGION = 20;
	public const float FIRE_MAX_STRETCH = 0.52f;
	public const float FIRE_MIN_STRETCH = 0.26f;
	public const float FIRE_Y_OFFSET = 0.3f;
	public const float FIRE_X_OFFSET = 0.3f;
	public const float FIRE_HEIGHT = 0.04f;
	public const int FIRE_ROTATION_SPEED = -90;
	public const int MONSTER_CHANCE = 5;
	public const string BACKGROUND_OBJECT_NAME = "gameBackground";
	public const string TRAIN_OBJECT_NAME = "Train";
	public const string FIRE_OBJECT_NAME = "firePrefab";
	public const string ROCK1_OBJECT_NAME = "rockPrefab1";
	public const string MONSTER_OBJECT_NAME = "monsterPrefab";
	public const string COIN_OBJECT_NAME = "coinPrefab";
	public const string ROCK_OBJECT_COLLIDER_NAME = "Sphere";
	public const string PAUSE_OBJECT = "pauseButton";
	public const string RESTART_OBJECT = "restartButton";
	public const string RESUME_OBJECT = "resumeButton";
	public const string PLANE_POWER_OBJECT = "planeUp";
	public const string LIFE_POWER_OBJECT = "lifeUp";
	public const string MENU_SCENE = "menuScene";
	public const string CONTROL_CIRCLE_NAME = "controlCircle";
	public const string MAIN_SCENE = "mainScene";
	public const string GAME_PANEL = "gamePanel";
	public const string PAUSE_PANEL = "pausePanel";
	public const string COIN_LABEL = "coinsGameLabel";
	public const string MENU_LABEL = "menuMessage";
	public const string CENTER_NAME = "GameCenter";
	public const string COIN_PREFAB_LOCATION = "Prefabs/coinPrefab";
	public const string FIRE_PREFAB_LOCATION = "Prefabs/firePrefab";
	public const string ACHIEVMENT_PREFAB_LOCATION = "Prefabs/Achievment";
	public const string ROCK1_DATA = "Prefabs/rockPrefab1";
	public const string LPARTICLE_DATA = "Prefabs/thrusterParticleLPrefab";
	public const string RPARTICLE_DATA = "Prefabs/thrusterParticleRPrefab";
	public const string WAGON_DATA = "Prefabs/wagonPrefab";
	public const string EMPTY_EVENT_DATA = "Prefabs/RandomEvents/randomEmptyEventPrefab";
	public const string FIRE_EVENT_DATA = "Prefabs/RandomEvents/randomEventFirePrefab";
	public const string COINS_EVENT_DATA = "Prefabs/RandomEvents/randomEventCoinsPrefab";
	public const string MONSTER_EVENT_DATA = "Prefabs/RandomEvents/randomEventMonster";
	public const string MAP_EVENT1_DATA = "Prefabs/EventMaps/FireEvent1";
	public const string MAP_EVENT2_DATA = "Prefabs/EventMaps/FireEvent2";
	public const string MAP_EVENT3_DATA = "Prefabs/EventMaps/FireEvent3";
	public const string MAP_COINS1_DATA = "Prefabs/EventMaps/EventCoins1";
	public const string MAP_COINS2_DATA = "Prefabs/EventMaps/EventCoins2";
	public const string MAP_COINS3_DATA = "Prefabs/EventMaps/EventCoins3";
	public const string MAP_COINS4_DATA = "Prefabs/EventMaps/EventCoins4";
	public const string MONSTER_DATA = "Prefabs/monsterPrefab";
	public const string PLANE_DATA = "Prefabs/planePrefab";
	public const string MISSILE_DATA = "Prefabs/missilePrefab";
	public const string LOADED_TEXT = "Click to play...";
	public const string MONSTER_SOUND = "monster";
	public const string SHOOT_SOUND = "shoot";
	public const string DAMAGE_SOUND = "damage";
	public const string COIN_SOUND = "coin";
	public const string GAME_ATLAS_TEXTURE_NAME = "gameplayPng";
	public const string GAME_ATLAS_JSON_NAME = "gameplay";
	public const string BACKGROUND_TEXTURE_LOCATION = "Atlas/background";
	public const string BUTTON_TEXTURE = "buttonpng.png";
	public const string FIRE_TEXTURE = "fire.png";
	public const string COIN_TEXTURE = "nugget.png";
	public const string ROCK1_TEXTURE = "rock1.png";
	public const string WAGON_TEXTURE = "wagon.png";
	public const string WHEEL_TEXTURE = "wheel.png";
	public const string MONSTER_TEXTURE = "monster.png";
	public const string OBSTACLE_TAG = "OBSTACLE";
	public const string UI_TAG = "UI";
	public const string FINISH_TAG = "Finish";
	public const string INIT_MESSAGE = "init";
	public const string XML_DATA_NAME = "Data.xml";
	public const string HUD_NAME = "00. Static HUD";
	
	
	
	
	// Returns a floating point random number
	// @param pMin - minimum number range
	// @param pMAx - maximum number range
	public static float getFloatRandom (float pMin, float pMax)
	{
		return _rnd.Next ((int)(pMin * 100), (int)(pMax * 100)) / 100f;	
	}
	
	// gets a random true/false
	public static bool getRandomBool ()
	{
		return  _rnd.Next () % 2 == 0;
	}

		
}


