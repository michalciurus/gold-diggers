﻿// @file CPlaneModel.cs
//
using UnityEngine;
using System.Collections;

public class CPlaneModel : MonoBehaviour
{

//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private float _currentAngle;
	private GameObject _circlingOver;
	private bool _doCircle;
	private bool _readyToshoot;
	private CTimer _shootTimer;

//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	
	// Use this for initialization
	void Awake ()
	{
		this._circlingOver = null;
		this._currentAngle = 0;
		this._doCircle = false;
		this._readyToshoot = true;
		this._shootTimer = new CTimer ();
		
	}
	
	// set texture
	void Start ()
	{
		this.gameObject.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (CTools.WHEEL_TEXTURE);
		this.gameObject.transform.parent = GameObject.Find (CTools.CENTER_NAME).transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		_shootTimer.UpdateTimer ();
		
		if (this._doCircle) {
			this.keepCircling ();	
		}
		
		shootIfReady();
	}
	
	// checking if it's time to shoot
	void shootIfReady ()
	{
		if (this._readyToshoot == true) {
			GameObject objectToshoot = this.findClosestObstaclePosition ();
			if (objectToshoot != null)
				this.shootAt (objectToshoot);
			// setting the timer for next shot
			this._shootTimer.setTimedCallback (this.readyToShoot, CTools.SHOOT_DELAY);
			this._readyToshoot = false;
		}
	}
	
		// calculating next frame position
	private void keepCircling ()
	{
		_currentAngle += (float)CTools.PLANE_CIRCLE_SPEED * Time.deltaTime;
		Vector3 objectTransform = this.gameObject.transform.position;
		objectTransform.x = this._circlingOver.transform.position.x + Mathf.Sin (this._currentAngle) * CTools.PLANE_DISTANCE;
		objectTransform.y = this._circlingOver.transform.position.y + Mathf.Cos (this._currentAngle) * CTools.PLANE_DISTANCE;
		this.gameObject.transform.position = objectTransform;
	}
	
	// Finding which obstacle is closest to destroy
	public GameObject findClosestObstaclePosition ()
	{
		float closestY = float.MinValue;
		GameObject actualClosestObject = null;
		GameObject background = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		
		// getting all the objects with tag = "OBSTACLE"
		foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag(CTools.OBSTACLE_TAG)) {
			Vector3 objectPosition = gameObject.transform.position;
			// If it's over the plane or not on camera
			if (objectPosition.y > this.gameObject.transform.position.y - CTools.PLANE_VIEW_OFFSET || objectPosition.y < background.transform.position.y - background.renderer.bounds.extents.y)
				continue;
			// changing if it's closer
			if (objectPosition.y - CTools.PLANE_VIEW_OFFSET > closestY) {
				closestY = objectPosition.y;
				actualClosestObject = gameObject;
				gameObject.tag = "Untagged";
			}
			
		}
		return actualClosestObject;
		
	}
	
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	
	// Setting an object that this plane should circle around
	// @param pObjectToCircleOver
	public void setObjectToCircleOver (GameObject pObjectToCircleOver)
	{
		this._circlingOver = pObjectToCircleOver;	
		this.gameObject.transform.position = this._circlingOver.transform.position;
		this._doCircle = true;
	}
	
	public void readyToShoot ()
	{
		this._readyToshoot = true;
	}
	
	// Generating a missile
	// @param pObjectToShoot - object to be destroyed
	public void shootAt (GameObject pObjectToShoot)
	{
		// playing the sound
		CSoundManager.soundManagerInstance.playShootSound ();
		// getting the misile
		GameObject missile = MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.MISSILE_DATA)) as GameObject;
		CMissileModel missileModel = missile.GetComponent (typeof(MonoBehaviour)) as CMissileModel;
		// setting the target
		missileModel.setTarget (this.gameObject.transform.position, pObjectToShoot);
	}
	
}
