﻿// @file CFireModel.cs
// used for the unity editor to save rotation speed as a public variable

using UnityEngine;
using System.Collections;

public class CFireModel : MonoBehaviour {
	// For the editor to save rotation speed in order to create maps
	public int _rotationSpeed;
}
