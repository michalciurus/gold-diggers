// @file Ctrain.cs
// train model

using UnityEngine;
using System;
using System.Collections.Generic;

public class CTrain : MonoBehaviour
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	
	public enum trainState
	{
		NORMAL,
		HURT,
		INVINCIBLE
	};

	public enum movementState
	{
		FREE,
		PUSHED
	};
	
	private int _numberOfWagons;
	private float _maxWagonVelocity;
	private float _currentTargetX;
	private float _lastForceApplied;
	private trainState _trainState;
	private movementState _movementState;
	private CTimer _timer;
	private GameObject _mainWagon;
	// index 0 is always the first wagon
	private	List<CWagonModel> _wagonList;
	private List<CPlaneModel> _planeList;
	private bool _changeToRed;
	private bool _componentActive;
	
//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	
	/*
	*loading the wagons and setting up the dependencies
	*/
	private void setWagons ()
	{
		// The first wagon
		this._wagonList.Add (((MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.WAGON_DATA)) as GameObject).GetComponent (typeof(MonoBehaviour)) as CWagonModel));		
		this._mainWagon = _wagonList [0].getWagonObject ();
	//	this._mainWagon.transform.localPosition = Vector3.zero;
		this._mainWagon.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (CTools.WAGON_TEXTURE);
		this._numberOfWagons++;
		this._mainWagon.transform.parent = this.gameObject.transform;
		// The rest of the wagons
		for (int i = 1; i < CTools.NUMBER_OF_WAGONS; i++) {
			this.addWagon ();
		}
	}
	


	
	
	/*
	 * Calculates an initial wagon position
	 * @param wagonIndexNumber which wagon in the array
	 */
	private Vector3 calculateInitWagonPosition (int pWagonIndexNumber)
	{	
		Vector3 vectorToReturn = new Vector3 ();
		GameObject previousWagon;
		previousWagon = this._wagonList [pWagonIndexNumber - 1].getWagonObject ();
		vectorToReturn.y += previousWagon.transform.position.y + CTools.WAGON_OFFSET_POSITION;
		vectorToReturn.x = previousWagon.transform.position.x;
		return vectorToReturn;
	}
	

	
	/*
	 * The train will slow down and change it's angle to simulate gravity
	 */
	private void simulateGravity ()
	{
		
		Vector3 wagonVelocity = this._mainWagon.rigidbody.velocity;
		Vector3 wagonAngle = this._mainWagon.transform.eulerAngles;
		
		if (wagonVelocity.x > 0) {
			wagonVelocity.x -= CTools.SPEED_SLOW_DOWN * Time.deltaTime;
		}

		if (wagonVelocity.x < 0) {
			wagonVelocity.x += CTools.SPEED_SLOW_DOWN * Time.deltaTime;
		}
		
		float decreaseAngle = CTools.SPEED_SLOW_DOWN * 10;
		if (wagonAngle.z - + decreaseAngle > 0 && wagonAngle.z + + decreaseAngle < CTools.MAX_WAGON_ANGLE) {
			wagonAngle.z -= decreaseAngle;

		} else if (wagonAngle.z + decreaseAngle > 360 - CTools.MAX_WAGON_ANGLE && wagonAngle.z - decreaseAngle < 360) {
			wagonAngle.z += decreaseAngle;

		}
		this._mainWagon.transform.eulerAngles = wagonAngle;
		this._mainWagon.rigidbody.velocity = wagonVelocity;
	}
	
	/*
	 * Happens when the train reaches it's x destination (finger position)
	 */
	public void stopWagon ()
	{
		this._mainWagon.rigidbody.velocity = Vector3.zero;	
	}
	
	/*
	 * Calculates maximum train velocity to calculate it's max angle 
	 */
	private float calculateMaxVelocity ()
	{
		float backgroundx = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME).renderer.bounds.extents.x * 2;
		float maxForce = backgroundx;
		return maxForce;
	}
	
	// The train blimps red indicating damage
	private void blimpRed (bool damageColor)
	{
		Color curColor = this._mainWagon.renderer.material.color;
		if (this._changeToRed) {
			if ( damageColor)
			{
				curColor.g -= CTools.COLOR_CHANGE_SPEED;	
			}
			else
			{
				curColor.r -= CTools.COLOR_CHANGE_SPEED;
			}
				curColor.b -= CTools.COLOR_CHANGE_SPEED;	
			
		} else {
			if ( damageColor)
			{
				curColor.g += CTools.COLOR_CHANGE_SPEED;	
			}
			else
			{
				curColor.r += CTools.COLOR_CHANGE_SPEED;
			}
				curColor.b += CTools.COLOR_CHANGE_SPEED;	
		}
		
		this._mainWagon.renderer.material.color = curColor;
	
		for (int i = 1; i < _wagonList.Count; i++) {
			this._wagonList [i].renderer.material.color = curColor;
		}
	
			
		if (curColor.g < 0)
			this._changeToRed = false;
		else if (curColor.g > 1)
			this._changeToRed = true;
		
	}
	
	private bool mouseIdle ()
	{
		 return ! CGameplayController.touchDown;
	}
	
	// set wagon to color white
	private void setColorWhite ()
	{
		foreach (CWagonModel wagon in this._wagonList) {
			wagon.renderer.material.color = Color.white;
		}
		
	}
	
	public void clear ()
	{
		foreach (CWagonModel model in this._wagonList) {
			Destroy (model.getWagonObject ());
		}
		foreach (CPlaneModel model in this._planeList) {
			Destroy (model.gameObject);	
		}
		
		CMissileModel[] arrayOfMissile =  GameObject.FindObjectsOfType(typeof(CMissileModel)) as CMissileModel[];
		
		foreach(CMissileModel missile in arrayOfMissile)
			Destroy(missile.gameObject);
	}
	
	public void setInvincible()
	{
		this._trainState = trainState.INVINCIBLE;
		this._timer.setTimedCallback (this.trainReadyToTakeDamage, CTools.TIME_TO_RECOVER);
	}
	
	
	
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	
	
		public void init()
	{
		this._componentActive = true;
		this._trainState = trainState.NORMAL;
		this._movementState = movementState.FREE;
		this._mainWagon = null;
		this._wagonList = new List<CWagonModel> ();
		this._planeList = new List<CPlaneModel> ();
		this.setWagons ();
		this._timer = new CTimer ();
		this._maxWagonVelocity = this.calculateMaxVelocity ();
		this._wagonList [0].createThrustersAtWagon ();
		this._changeToRed = true;
		this._mainWagon.transform.localPosition = Vector3.zero;
	}
	
		public void release()
	{
		this.clear();
		this._componentActive = false;
		
	}
	
	/*
	* Called every from to update the train
	*/
	
	public void Start()
	{
		this._componentActive = false;
	}
	
	
	public void Update ()
	{
		if ( !this._componentActive)
			return;
		
		this._timer.UpdateTimer ();
		
		if (this._trainState == trainState.HURT) {
			this.blimpRed (true);	
		}
		else if ( this._trainState == trainState.INVINCIBLE)
		{
			this.blimpRed (false);	
		}
		
		if (this.mouseIdle () || this._movementState == movementState.PUSHED) {
			this.simulateGravity ();
			if (this._mainWagon.rigidbody.velocity.x != 0) {
				if (this._mainWagon.transform.position.x > this._currentTargetX - 0.04 && this._mainWagon.transform.position.x < this._currentTargetX + 0.04) {
					this.stopWagon ();
					if (this._movementState == movementState.PUSHED) {
						Debug.Log ("SETTING TO NORMAL");
						this._movementState = movementState.FREE;
					}
				}
			}	
		}
	}
	
	// A train is pushed because it hit a rock
	// @param pMoveVector - push force
	public void pushTrain (Vector3 pMoveVector)
	{
		this.moveTrainInput (pMoveVector);
		this._movementState = movementState.PUSHED;
	}
	
	/* Input recognized
	* @param pInputVector - mouse position vector
	*/ 
	public void moveTrainInput (Vector3 pMoveVector)
	{	// checking if out of bounds



		if (this._movementState != movementState.PUSHED) {
			this._currentTargetX = pMoveVector.x;
			float force = pMoveVector.x - this._mainWagon.rigidbody.position.x;
			this.applyXforce (force);
		}

	}
	
	/* apply the x force to the wagon
	 * @param force which is to be applied to the wagon
	 */ 
	public void applyXforce (float pForce)
	{	
		this.stopWagon ();
		this._lastForceApplied = pForce;
		this._mainWagon.rigidbody.AddForce (new Vector3 (pForce * CTools.VELOCITY_MULTIPLIER, 0, 0));	
		float calculateAngle = CTools.MAX_WAGON_ANGLE / this._maxWagonVelocity * pForce;
		this._mainWagon.transform.eulerAngles = new Vector3 (0, 0, calculateAngle);
		
	}

	// _mainWagon getter
	public GameObject getMainWagon ()
	{
		return this._mainWagon;	
		
	}
	
	// 
	public void removeLastWagon ()
	{		
		this._numberOfWagons--;
		this._wagonList[this._wagonList.Count - 1].setFollowed(false);
		GameObject toDelete = this._wagonList [this._wagonList.Count - 1].getWagonObject ();
		this._wagonList.RemoveAt (this._wagonList.Count - 1);
		
		GameObject.Destroy (toDelete);
		
	}
	
	// state setter
	public void setState (CTrain.trainState _state)
	{
		if (_state == trainState.NORMAL) {
			this.setColorWhite ();
		}
		
		this._trainState = _state;	
		
	}
	
	// simulate train taking damage
	public void trainTakeDamage()
	{
			CSoundManager.soundManagerInstance.playDamageSound ();
			// set timer to set train to normal mode
			this._timer.setTimedCallback (this.trainReadyToTakeDamage, CTools.TIME_TO_RECOVER);
				removeLastWagon ();
				setState (CTrain.trainState.HURT);
	}
	
	// add a wagon at the end
	public void addWagon ()
	{
		this._wagonList.Add ((MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.WAGON_DATA))as GameObject).GetComponent (typeof(MonoBehaviour)) as CWagonModel);
		this._wagonList [this._wagonList.Count - 1].getWagonObject ().transform.position = this.calculateInitWagonPosition (this._wagonList.Count - 1);
		// Which wagon to follow
		this._wagonList [this._wagonList.Count - 1].setFollow (this._wagonList [this._wagonList.Count - 2]);
		this._wagonList [this._wagonList.Count - 1].renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (CTools.WAGON_TEXTURE);
		this._wagonList [this._wagonList.Count - 1].transform.parent = this.gameObject.transform;
	}
	
	public int getNumberOfWagons ()
	{
		return this._wagonList.Count;
	}
	
	public CTrain.trainState getState ()
	{
		return this._trainState;	
	}

	public void clearAndRestart ()
	{
		this.clear ();
		this.init();
	}
	
	public float getCurrentTargetX()
	{
		return this._currentTargetX;
	}
	
	// add a plane to the wagon
	public void addPlane ()
	{
		GameObject plane = MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.PLANE_DATA)) as GameObject;
		CPlaneModel planeModel = (plane.GetComponent (typeof(MonoBehaviour))) as CPlaneModel;
		this._planeList.Add (planeModel);
		planeModel.setObjectToCircleOver (this._mainWagon);

		this._timer.setTimedCallback (this.removePlane, CTools.PLANE_TIME);
	}
	
	// remove a plane
	public void removePlane ()
	{
		CPlaneModel planeToRemove = this._planeList [this._planeList.Count - 1];
		this._planeList.Remove (planeToRemove);
		Destroy (planeToRemove.gameObject);
	}
	
	public void trainReadyToTakeDamage ()
	{
		setState (CTrain.trainState.NORMAL);
	}
	
	public void setComponentActive(bool val)
	{
		this._componentActive = val;
	}
	
	
		
}


