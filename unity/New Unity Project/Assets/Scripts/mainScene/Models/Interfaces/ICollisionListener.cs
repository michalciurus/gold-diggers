// @file ICollisionListener.cs
// collision listener for the wagon for the system to interpret

using System;
using UnityEngine;

	public interface ICollisionListener
	{
	void coinCollision(GameObject pCoinObject);
	void rockCollision(GameObject pRockObject);
	void fireCollision(GameObject pFireObject);
	void monsterCollision(GameObject pMonsterObject);
	void planeUpCollision(GameObject pPlaneUp);
	void lifeUpCollision(GameObject pLifeUp);
	}


