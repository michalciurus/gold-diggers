// @file IFreeFallGameModel.cs
// Model component interface

using System;
using UnityEngine;


	public interface IFreeFallGameModel
	{
		void inputOnPosition (Vector3 pInputVector);
		void updateModel();
		void restartGame();
		void pauseGame();
		void startGame();
		float getCurrentTargetX();
		int getCoins();
		void addLife();
		void addPlane();
		void addInvi();
		CTrain.trainState getTrainState();
	
	}


