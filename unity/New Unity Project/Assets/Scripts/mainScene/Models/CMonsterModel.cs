﻿// @file CMonsterModel
// 

using UnityEngine;
using System.Collections;

public class CMonsterModel : MonoBehaviour {
//------------------------------------------------------
// Private Variables
//------------------------------------------------------

	private bool _playedSound;

//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	// Use this for initialization
	void Start () {
	this._playedSound = false;
	}
	
	// Update is called once per frame
	// Checking if a sound is to be played
	void Update () {
		// checking if ready to play the sound of monster
		// if the monster is on the screen
		if ( ! this._playedSound && this.transform.position.y > GameObject.Find(CTools.BACKGROUND_OBJECT_NAME).transform.position.y - GameObject.Find (CTools.BACKGROUND_OBJECT_NAME).renderer.bounds.extents.y)
		{
		CSoundManager.soundManagerInstance.playMonsterSound();
		this._playedSound = true;	
		}
	
	}
}
