﻿/*
* @file CGameplayController.cs
* Implementing MVC pattern. This class is the main controller in the pattern. It reads input data and responds by changing model.
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CFreeFallGameplayModel : MonoBehaviour, ICollisionListener
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	
	// A list of scrolling random events
	private List<CRandomEvent> _randomEvents;
	private List<CRandomEventSingle> _singleEvents;
	
	private int _numberOfCoins;
	private bool _componentActive;
	private CTrain _mainTrain;
	private GameObject _backgroundObject;
	private CTimer _timer;
	
	private bool _gameActive;
	private bool _gamePaused;
	private bool _showMenuFlag;
//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	/*
	 * Used to calculate camera 2d height and width in world coordinates
	 */
	// Generates a random events - either randomly generated or preloaded
	private void addRandomEvent ()
	{
		CRandomEvent eventToAdd;
		// Which event is going to happen ?
		int calculateChance = CTools._rnd.Next (0, 100);
		
		// Calculating the chance for COIN event
		if (calculateChance > 0 && calculateChance <= CTools.COINS_CHANCE) {
			eventToAdd = CRandomEventFactory.getRandomEvent(CRandomEventFactory.ERandomEventType.COINS);
			/// Fire event
		} else if (calculateChance >= CTools.COINS_CHANCE && calculateChance <= CTools.COINS_CHANCE + CTools.FIRE_CHANCE) {
			eventToAdd = CRandomEventFactory.getRandomEvent(CRandomEventFactory.ERandomEventType.FIRE);			
		}
		// Pick an empty event
		else {
			eventToAdd = CRandomEventFactory.getRandomEvent(CRandomEventFactory.ERandomEventType.EMPTY);
		}
		this._randomEvents.Add(eventToAdd);
	}
	// destroys events and clears lists
	private void destroyAllEvents ()
	{
		foreach (CRandomEvent randEv in _randomEvents) {
			GameObject.Destroy (randEv.gameObject);	
		}
		_randomEvents.Clear ();
		
		foreach(CRandomEventSingle randEv in _singleEvents)
		{
			GameObject.Destroy (randEv.gameObject);	
		}
		_singleEvents.Clear();
	}
	
	// checks if the first event on the list is off the background. 
	private void destroyEventOverBackground()
	{
		if ( this._randomEvents.Count == 0)
			return;
		
		float lastRandomEventYPos = this._randomEvents [0].getGroup ().transform.position.y;
		 // if element is off background, dispose of it
		if (lastRandomEventYPos >= this._backgroundObject.transform.position.x + this._backgroundObject.renderer.bounds.extents.y * 2) {
			GameObject.Destroy (_randomEvents [0].getGroup ());
			_randomEvents.Remove (_randomEvents [0]);
		}	
	}
	
	private void checkForSingleEventsOverBackground()
	{
		CRandomEventSingle toRemove = null;
		
		foreach ( CRandomEventSingle singEv in this._singleEvents)
		{
			float lastRandomEventYPos = singEv.getGroup ().transform.position.y;
			if (lastRandomEventYPos >= this._backgroundObject.transform.position.x + this._backgroundObject.renderer.bounds.extents.y * 2)
			{
				toRemove = singEv;
				break;
			}
		}
		
		if (toRemove != null)
		{
		GameObject.Destroy( toRemove.getGroup());
		this._singleEvents.Remove(toRemove);
		}
		
		
	}
	
	// sets damage to the train
	private void setTrainDamage ()
	{
		// only if the train is in the normal state
		if (this._mainTrain.getState () == CTrain.trainState.NORMAL) {
			
			if (this._mainTrain.getNumberOfWagons () > 1) {
				this._mainTrain.trainTakeDamage();
			} else {
				// this means that there is less than 2 trains so the train is destroyed
				this.finishedGame();
			//	this.restartGame ();
			}
		}
	}
	
	private void generateSingleEvent()
	{
		// Generating single events
		int singleEventChance = CTools._rnd.Next (1000);
		if (singleEventChance <= CTools.MONSTER_CHANCE)
		this._singleEvents.Add(	CRandomEventFactory.getRandomEvent(CRandomEventFactory.ERandomEventType.SINGLE) as CRandomEventSingle);
	
	}
	// is event ready to be fired ?
	private bool readyToFireEvent()
	{
		if(this._randomEvents.Count == 0)
			return true;
		
		float firstRandomEventYPos = this._randomEvents [_randomEvents.Count - 1].getGroup ().transform.position.y;
		// Are events ready to be fired ?
		// either if the list is empty or if the last element added is on the screen
		return (_randomEvents.Count == 0 || 
		firstRandomEventYPos - CTools.RANDOM_REGION / 2 >= this._backgroundObject.transform.position.y - this._backgroundObject.renderer.bounds.extents.y);
	}
	
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	
	
	public void Start()
	{
		this._componentActive = false;
	}
	
	public void init()
	{
		this._componentActive = true;
		_gameActive= false;
		_timer = new CTimer ();
		_showMenuFlag = false;
		_mainTrain = GameObject.Find (CTools.TRAIN_OBJECT_NAME).GetComponent (typeof(MonoBehaviour)) as CTrain;
		_mainTrain.init();
		this._numberOfCoins = 0;
		_randomEvents = new List<CRandomEvent> ();
		this._singleEvents = new List<CRandomEventSingle>();
		this._backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		this.startGame();
		_timer.setTimedCallback(setGameActive,2);
		
		(this._mainTrain.getMainWagon ().GetComponent (typeof(MonoBehaviour)) as CWagonModel).setCollisionListener (this);
	}
	
	public void coinCollision (GameObject pCoinObject)
	{
		CSoundManager.soundManagerInstance.playCoinSound ();
		this._numberOfCoins++;
		GameObject.DestroyObject (pCoinObject);
	}
	
	public void setGameActive()
	{
		_gameActive = true;
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("START");
	}

	public void rockCollision (GameObject pRockObject)
	{
		if (this._mainTrain.getState () == CTrain.trainState.NORMAL) {
			this.setTrainDamage ();
			if (pRockObject.transform.position.x < (GameObject.Find (CTools.BACKGROUND_OBJECT_NAME) as GameObject).transform.position.x) {
				// pushing the train from the rock depending if the rock is on the let or right
				float pushForce = pRockObject.transform.position.x + pRockObject.renderer.bounds.extents.x + _mainTrain.getMainWagon().renderer.bounds.extents.x + CTools.BOUNCE_OFF_ROCK_OFFSET;
				this._mainTrain.pushTrain (new Vector3 (pushForce, 0, 0));
			} else {
				float pushForce = pRockObject.transform.position.x - pRockObject.renderer.bounds.extents.x - _mainTrain.getMainWagon ().renderer.bounds.extents.x - CTools.BOUNCE_OFF_ROCK_OFFSET;
				this._mainTrain.pushTrain (new Vector3 (pushForce, 0, 0));
			}
		
		}
	}
	
	public void planeUpCollision (GameObject pPlaneUp)
	{
		this._mainTrain.addPlane ();	
		GameObject.Destroy (pPlaneUp);
		
	}
	
	public void lifeUpCollision (GameObject pLifeUp)
	{
		this._mainTrain.addWagon ();
		GameObject.Destroy (pLifeUp);	
	}
	
	public void monsterCollision (GameObject pMonsterObject)
	{
		this.setTrainDamage ();	
	}

	public void fireCollision (GameObject pFireObject)
	{
		this.setTrainDamage ();
	}
	

	
	// Update random system on every frame
	public void updateModel ()
	{
			if ( _showMenuFlag)
		{
			(this.GetComponent<CGameplayView>() as CGameplayView).finishGame();
			_showMenuFlag = false;	
		}
		
		if ( _gamePaused )
			return;
		
		if ( !_componentActive )
			return;
		
		this._timer.UpdateTimer ();
		

		
		if ( this._gameActive)
		{
		
		this.destroyEventOverBackground();
		
		this.generateSingleEvent();
		
		this.checkForSingleEventsOverBackground();
		
		if (this.readyToFireEvent())
		this.addRandomEvent();
		}
	}
	
	public void inputOnPosition (Vector3 pMoveVector)
	{
		this._mainTrain.moveTrainInput (pMoveVector);
	}
	
	// restarts the game
	public void restartGame ()
	{
		// resets the coin
		this._numberOfCoins = 0;
		// clears the level
		destroyAllEvents ();
		// restarts the wagon
		this._mainTrain.clearAndRestart ();
		// sets the listener
		(this._mainTrain.getMainWagon ().GetComponent (typeof(MonoBehaviour)) as CWagonModel).setCollisionListener (this);
		this.startGame ();
	}
	
	public void release()
	{
		// resets the coin
		this._numberOfCoins = 0;
		// clears the level
		destroyAllEvents ();
		_gameActive = false;
		(GameObject.Find(CTools.HUD_NAME).GetComponent<HUDController>() as HUDController).showMessage("");
		_mainTrain.release();		
	}
	
	public void finishedGame()
	{
		int newNumber = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfCoins) + this._numberOfCoins;
		XMLGameData.XMLGameDataInstance._numberOfCoins = newNumber.ToString();
		XMLGameData.XMLGameDataInstance.Save();
		this.pauseGame();
		_showMenuFlag = true;
	}
	
	public int getCoins ()
	{
		return this._numberOfCoins;	
	}

	public void pauseGame ()
	{
		_gamePaused = true;
		Time.timeScale = 0;
	}
	
	public void startGame ()
	{
		_gamePaused = false;
		Time.timeScale = 1;
	}
	
	public CTrain getMainTrain ()
	{
		return this._mainTrain;	
	}
	
	public float getCurrentTargetX()
	{
		return this._mainTrain.getCurrentTargetX();
	}
	
	public void addLife()
	{
		_mainTrain.addWagon();	
	}
	
	public void addInvi()
	{
		_mainTrain.setInvincible();	
	}
	
	public void addPlane()
	{
		_mainTrain.addPlane();	
	}
	
	public CTrain.trainState getTrainState()
	{
		return _mainTrain.getState();
	}
	
}
