﻿// @file CMissileModel.cs
// model for a targeted missile

using UnityEngine;
using System.Collections;

public class CMissileModel : MonoBehaviour
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private Vector3 _to;
	private Vector3 _from;
	private bool _missileActive;
	private GameObject _objectToDestroy;
	//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	void Awake ()
	{
		this._missileActive = false;
	}

	void Update ()
	{
		
		if (this._missileActive) {
			this.moveOverTheLine ();
		}
	}
	
	// Incrementing the missiles position over a line 
	void moveOverTheLine()
	{
		// calculating the next position
		float speed = CTools.GUIDED_MISSILE_SPEED * Time.deltaTime;
		if (_to.x < _from.x)
			speed *= -1;
		float nextX = this.gameObject.transform.position.x + speed;
		//using linear equation to calculate y
		float nextY = ((_to.y - _from.y) / (_to.x - _from.x)) * (nextX - _from.x) + _from.y;
		// applying the position change
		this.gameObject.transform.position = new Vector3 (nextX, nextY, this.gameObject.transform.position.z);	
		// checking if the missile has hit the target
		if ((speed < 0 && _to.x > nextX) || (speed > 0 && _to.x < nextX))
			this.hit ();
	}
	
	
	// Destroys the missile and destroys the object target
	void hit ()
	{	
		if (_objectToDestroy == null)
			return;
		// sending message to the event to take care of destroying the target
		(this._objectToDestroy.transform.parent.gameObject.GetComponent (typeof(MonoBehaviour)) as CRandomEvent).removeObstacle (this._objectToDestroy);
		Destroy (this.gameObject);
	}
	
	
//------------------------------------------------------
// Public Variables
//------------------------------------------------------
	
	// Set target for targeted missile
	// @param pFrom - position from which the missile is shot
	// @param pObjectToDestroy -
	public void setTarget (Vector3 pFrom, GameObject pObjectToDestroy)
	{
		// setting the data
		this._objectToDestroy = pObjectToDestroy;
		this._to = pObjectToDestroy.transform.position;
		this.gameObject.transform.position = pFrom;
		this._from = pFrom;
		this._missileActive = true;
		// setting the tag so other missile won't target this object
		pObjectToDestroy.tag = CTools.FINISH_TAG;
	}
	

	
	
	
}
