/*
* @file IRandomEvent.cs
* Interface for random events
*/
using System;
using UnityEngine;

// A random event is a group of GameObjects scrolling through the level.
public abstract class CRandomEvent : MonoBehaviour
{
	
//------------------------------------------------------
// Protected Variables
//------------------------------------------------------
	protected GameObject _parentObject;
	protected float _incrementSpeed;
//------------------------------------------------------
// Protected & Private Methods
//------------------------------------------------------
	
	protected void Awake ()
	{
		// getting the object from the script
		_parentObject = this.gameObject;
		this._incrementSpeed = CTools.ELEMENTS_SPEED;
		this.gameObject.transform.parent = GameObject.Find (CTools.CENTER_NAME).transform;
	}
	
	//Interface component
	protected virtual void incrementGroupY (float yCoord)
	{
		_parentObject.transform.position = new Vector3 (_parentObject.transform.position.x, _parentObject.transform.position.y + yCoord, _parentObject.transform.position.z);
	}
	
	// adds a element from prefabs 
	// @param pLocalX
	// @param prefabName
	// @param textureName
	protected GameObject addElement (float pLocalX, string prefabName, string textureName)
	{
		GameObject element = MonoBehaviour.Instantiate (Resources.Load (prefabName)) as GameObject;
		element.transform.position = new Vector3 (this._parentObject.transform.position.x + pLocalX, this._parentObject.transform.position.y + CTools.RANDOM_REGION / 2, this._parentObject.transform.position.z);
		element.transform.parent = this._parentObject.transform;
		element.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (textureName);
		return element;
	}
	
	private void Update ()
	{
		this.incrementGroupY (this._incrementSpeed * Time.deltaTime);
		GameObject background = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
	}
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	//Interface component
	public void setGroupY (float pYCoord)
	{
		_parentObject.transform.position = new Vector3 (_parentObject.transform.position.x, pYCoord, _parentObject.transform.position.z);
	}
	//Interface component
	public GameObject getGroup ()
	{
		return _parentObject;	
	}
	//Virtual method for removing obstacles
	// @param pObstacleToRemove
	public virtual void removeObstacle (GameObject pObstacleToRemove)
	{
		GameObject.Destroy (pObstacleToRemove);
	}

		
	
	

}




