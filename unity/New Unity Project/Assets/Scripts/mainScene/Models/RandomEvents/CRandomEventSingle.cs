// @file CRandomEventSingle.cs
// adding single events like monsters or powerups
using UnityEngine;
using System.Collections;

public class CRandomEventSingle : CRandomEvent
{
			
	private CRandomEventSingle ()
	{
	}
	
	public void addMonster (float pLocalX)
	{
		this._incrementSpeed = CTools.MONSTER_SPEED;
		GameObject monster = this.addElement (pLocalX, CTools.MONSTER_DATA, CTools.MONSTER_TEXTURE);
		monster.tag = CTools.OBSTACLE_TAG;
	}
	
	public void addPlanePowerup (float pLocalX)
	{
		this._incrementSpeed = CTools.ELEMENTS_SPEED;
		GameObject planePowerup = this.addElement (pLocalX, CTools.COIN_PREFAB_LOCATION, CTools.WHEEL_TEXTURE);
		planePowerup.transform.localScale = new Vector3 (CTools.POWERUP_SIZE, CTools.POWERUP_SIZE, planePowerup.transform.localScale.z);
		planePowerup.name = CTools.PLANE_POWER_OBJECT;
	}
	
	public void addLifePowerUp (float pLocalX)
	{
		this._incrementSpeed = CTools.ELEMENTS_SPEED;
		GameObject lifePowerup = this.addElement (pLocalX, CTools.COIN_PREFAB_LOCATION, CTools.WAGON_TEXTURE);
		lifePowerup.transform.localScale = new Vector3 (CTools.POWERUP_SIZE, CTools.POWERUP_SIZE, lifePowerup.transform.localScale.z);
		lifePowerup.name = CTools.LIFE_POWER_OBJECT;
	}
	
	
	
	

	

}

