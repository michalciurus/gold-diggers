﻿/*
* @file CRandomEventFire.cs
* Event that cointains fire and rock that scroll through the level.
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CRandomEventFire : CRandomEvent
{
	
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private List<GameObject> firesToRotate;
		
	
//------------------------------------------------------
// Protected & Private Methods
//------------------------------------------------------
	
	protected new void Awake ()
	{
		base.Awake ();
		firesToRotate = new List<GameObject> ();
	}
	
	protected override void incrementGroupY (float yCoord)
	{
		base.incrementGroupY (yCoord);
		
		foreach (GameObject gObject in firesToRotate) {
			gObject.transform.Rotate (new Vector3 (gObject.transform.eulerAngles.x, gObject.transform.eulerAngles.y, (gObject.GetComponent (typeof(MonoBehaviour))as CFireModel)._rotationSpeed * Time.deltaTime));
		}
		
	}
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	/*
		 * add a fire element to the random event
		 * @param xLocal -
		 * @param yLocal - 
		 * @param scaleX - 
		 * @param rotateSpeed - in degrees
		 */
	public void addFire (float xLocal, float yLocal, float scaleX, int rotateSpeed)
	{
		// setting up a fire prefab
		GameObject fire = MonoBehaviour.Instantiate (Resources.Load (CTools.FIRE_PREFAB_LOCATION)) as GameObject;
		fire.transform.position = new Vector3 (this._parentObject.transform.position.x + xLocal, this._parentObject.transform.position.y + yLocal, this._parentObject.transform.position.z);
		fire.transform.localScale = new Vector3 (scaleX, CTools.FIRE_HEIGHT, 1);
		fire.tag = CTools.OBSTACLE_TAG;
		fire.transform.parent = this._parentObject.transform;
		fire.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (CTools.FIRE_TEXTURE);
		if (rotateSpeed != 0)
			(fire.GetComponent (typeof(MonoBehaviour)) as CFireModel)._rotationSpeed = rotateSpeed;
		this.firesToRotate.Add (fire);		
	}
	/*
		 * add a rock element to the random event
		 * @param xLocal -
		 * @param yLocal - 
		 * @param pIvert - invert texture (right rock)
		 */
	public void addRock (float pXLocal, float pYLocal, bool pInvert)
	{
		GameObject rock = MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.ROCK1_DATA)) as GameObject;
		
		if (pInvert)
			rock.transform.localScale = new Vector3 (- rock.transform.localScale.x, rock.transform.localScale.y, rock.transform.localScale.z);
		rock.transform.position = new Vector3 (this._parentObject.transform.position.x + pXLocal, this._parentObject.transform.position.y + pYLocal, this._parentObject.transform.position.z); 
		rock.transform.parent = this._parentObject.transform;
		rock.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache (CTools.ROCK1_TEXTURE);
	}
	
	// removes an obstacle from the list and world
	// @param pObstacleToRemove -
	public override void removeObstacle (GameObject pObstacleToRemove)
	{
		if (this.firesToRotate.Contains (pObstacleToRemove))
			this.firesToRotate.Remove (pObstacleToRemove);
		base.removeObstacle (pObstacleToRemove);
	}
	
		
	
}
