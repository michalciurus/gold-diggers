﻿/*
* @file CEmptyRandomEvent.cs
* It's an empty place scrolling through the level.
*/
using UnityEngine;
using System.Collections;

// in the future this might hold various decoration items not affecting the gameplay
public class CEmptyRandomEvent : CRandomEvent {
	
}
