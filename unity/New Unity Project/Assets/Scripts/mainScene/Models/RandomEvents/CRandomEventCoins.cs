﻿/*
* @file CRandomEventCoins.cs
* Event that cointains coins that scroll through the level.
*/

using UnityEngine;
using System.Collections;


public class CRandomEventCoins : CRandomEvent {
//------------------------------------------------------
// Public Methods
//------------------------------------------------------

		// Adds a coin at given local coordinates
		// Parameters
		// xLocal -
		// yLocal -
		public void addCoin (float xLocal, float yLocal)
		{
			GameObject coin = MonoBehaviour.Instantiate (Resources.Load (CTools.COIN_PREFAB_LOCATION)) as GameObject;
			coin.transform.position = new Vector3 (_parentObject.transform.position.x + xLocal, _parentObject.transform.position.y + yLocal, _parentObject.transform.position.z);
			coin.transform.parent = this._parentObject.transform;
			coin.renderer.material.mainTexture = CGraphicManager.GameLoaderInstance.getTextureFromCache(CTools.COIN_TEXTURE);
		}
	
}
