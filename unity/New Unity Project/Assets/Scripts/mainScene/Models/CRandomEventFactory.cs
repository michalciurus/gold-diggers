using System;
using UnityEngine;
using System.Collections.Generic;

// A class that implements a factory pattern and returns random events
	public class CRandomEventFactory
	{	
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private static List<string> _listOfMaps;
	private static List<string> _listOfCoinMaps;
	
	static CRandomEventFactory()
	{
		CRandomEventFactory._listOfMaps = new List<string>();
		CRandomEventFactory._listOfCoinMaps = new List<string>();
		CRandomEventFactory.prepareListOfMaps();
	}
	
//------------------------------------------------------
// Public Variables
//------------------------------------------------------
	public enum ERandomEventType { COINS, EMPTY, FIRE, SINGLE };
	
//------------------------------------------------------
// Public Static Methods
//------------------------------------------------------
	
	public static CRandomEvent getRandomEvent( ERandomEventType pEventType)
	{
		CRandomEvent eventToReturn = null;
		// getting the background for calculation
		GameObject backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		
		if ( pEventType == ERandomEventType.COINS)
		{
			CRandomEventCoins coinsEvent = (MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.COINS_EVENT_DATA)) as GameObject).GetComponent (typeof(MonoBehaviour)) as CRandomEventCoins;
			int whichMapIndex = CTools._rnd.Next () % CRandomEventFactory._listOfCoinMaps.Count;	
			// randomly picking a map from the list
			CRandomEventFactory.addFromMap (CRandomEventFactory._listOfCoinMaps [whichMapIndex], coinsEvent);
			eventToReturn =  coinsEvent;
		}
		else if ( pEventType == ERandomEventType.EMPTY)
		{
			CEmptyRandomEvent fire = (MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.EMPTY_EVENT_DATA)) as GameObject).GetComponent (typeof(MonoBehaviour)) as CEmptyRandomEvent;
			eventToReturn = fire;
			eventToReturn =  fire;
		}
		else if ( pEventType == ERandomEventType.FIRE)
		{
			CRandomEventFire fire = (MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.FIRE_EVENT_DATA)) as GameObject).GetComponent (typeof(MonoBehaviour)) as CRandomEventFire;
		int	calculateChance = CTools._rnd.Next (0, 100);
			
			// Add randomly
			if (calculateChance > 0 && calculateChance <= 100) {
				CRandomEventFactory.addRandomFireElements (fire);
			} 

			// Add from map
			else {
				int whichMapIndex = CTools._rnd.Next () % CRandomEventFactory._listOfMaps.Count;	
				CRandomEventFactory.addFromMap (CRandomEventFactory._listOfMaps [whichMapIndex], fire);
			}
			eventToReturn =  fire;
			
		}
		else if ( pEventType == ERandomEventType.SINGLE)
		{
			eventToReturn = CRandomEventFactory.getSingleEvent();
		}
		
		eventToReturn.setGroupY (backgroundObject.transform.position.y - backgroundObject.renderer.bounds.extents.y - CTools.RANDOM_REGION);
		return eventToReturn;
	}
	
//------------------------------------------------------
// Private Static Methods
//------------------------------------------------------
	// randomly picks a single event : monster or a pick-up 
	private static CRandomEvent getSingleEvent ()
	{
		GameObject backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		CRandomEventSingle monsterEvent = (MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.MONSTER_EVENT_DATA)) as GameObject).GetComponent (typeof(MonoBehaviour)) as CRandomEventSingle;
		float monsterExtents = CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.MONSTER_DATA).renderer.bounds.extents.x;
		// generating x
		float x = (CTools.getFloatRandom (0 + monsterExtents, backgroundObject.renderer.bounds.extents.x * 2 - monsterExtents)) - backgroundObject.renderer.bounds.extents.x;
		// Choosing if pickup or monster
		if (CTools.getRandomBool ())
			monsterEvent.addMonster (x);
		else {
			if (CTools.getRandomBool ())
				monsterEvent.addPlanePowerup (x);
			else
				monsterEvent.addLifePowerUp (x);
		}
		return monsterEvent;
	}
	
	// Adds a preloaded map from prefabs as a random event
	// In future it'll be loaded from database and saved to text, most ideally with some game level editor developed
	// @param pPrefabName - map name
	// @param pRandomEvent - to which event the map is to be added
	private static void addFromMap (string pPrefabName, CRandomEvent pRandomEvent)
	{
		GameObject mapPrefab = CGraphicManager.GameLoaderInstance.getPrefabFromCache (pPrefabName);
		
		CRandomEventFire pRandomEventFire = pRandomEvent as CRandomEventFire;
		// getting children from prefab to add to event
		for (int i = 0; i <  mapPrefab.transform.childCount; i++) {
			GameObject child = mapPrefab.transform.GetChild (i).gameObject;
			
			// picking children and adding them to events
			// different maps for different classes
			if (pRandomEvent is CRandomEventFire) {
				if (child.name == CTools.FIRE_OBJECT_NAME) {
					(pRandomEvent as CRandomEventFire).addFire (child.transform.position.x, child.transform.position.y, child.transform.localScale.x, (child.GetComponent (typeof(MonoBehaviour)) as CFireModel)._rotationSpeed);
				}
				if (child.name == CTools.ROCK1_OBJECT_NAME) {
					bool isInverted = child.transform.localScale.x < 0;
					(pRandomEvent as CRandomEventFire).addRock (child.transform.position.x, child.transform.position.y, isInverted);
				}
			} else if (pRandomEvent is CRandomEventCoins) {
				(pRandomEvent as CRandomEventCoins).addCoin (child.transform.position.x, child.transform.position.y);
			}
		}	
	}
	
	// Getting a random rock pattern from all patterns
	// @param pRandomFireEvent - to which event are rocks to be added
	private static List<Bounds> addRandomRockPattern (CRandomEventFire pRandomFireEvent)
	{	
		GameObject backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);		
		Bounds rockBounds = CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.ROCK1_DATA).renderer.bounds;

		bool isLeftActive = CTools.getRandomBool ();
		bool isRightActive = CTools.getRandomBool ();
		
		Vector3 tempVector = new Vector3 ();
		tempVector.z = 3;
		
		List<Bounds> rockBoundsList = new List<Bounds> ();
		
		// generating and adding left rock
		if (isLeftActive) {
			Bounds leftRockBounds = new Bounds ();
			// generatnig y positiong
			tempVector.y = CTools.getFloatRandom (0 + rockBounds.extents.y, CTools.RANDOM_REGION - rockBounds.extents.y);
			// setting x position
			tempVector.x = - backgroundObject.renderer.bounds.extents.x + rockBounds.extents.x;
			leftRockBounds.center = tempVector;
			tempVector.x = rockBounds.extents.x;
			tempVector.y = rockBounds.extents.y;
			leftRockBounds.extents = tempVector;
			pRandomFireEvent.addRock (leftRockBounds.center.x, leftRockBounds.center.y, false);		
			rockBoundsList.Add (leftRockBounds);	
		}	
		// generating and adding right rock
		if (isRightActive) {
			Bounds rightRockBounds = new Bounds ();
			tempVector.y = CTools.getFloatRandom (0 + rockBounds.extents.y, CTools.RANDOM_REGION - rockBounds.extents.y);
			tempVector.x = + backgroundObject.renderer.bounds.extents.x - rockBounds.extents.x;
			rightRockBounds.center = tempVector;
			tempVector.x = rockBounds.extents.x + CTools.FIRE_X_OFFSET / 2;
			tempVector.y = rockBounds.extents.y + CTools.FIRE_Y_OFFSET / 2;
			rightRockBounds.extents = tempVector;
			pRandomFireEvent.addRock (rightRockBounds.center.x, rightRockBounds.center.y, true);		 
			rockBoundsList.Add (rightRockBounds);
		}
		return rockBoundsList;
	}
	
	/* 
	 * Very simple algorithm of adding fire elements to a random event
	 * @param pRandomFireEvent - event in which the elements will be added
	 */
	private static void addRandomFireElements (CRandomEventFire pRandomFireEvent)
	{
		GameObject backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		List<Bounds> rocksList = CRandomEventFactory.addRandomRockPattern (pRandomFireEvent);
		// extents are half the size
		int howMany = CTools._rnd.Next (1, CTools.MAX_FIRE_PER_REGION);
		List<Bounds> listOfFire = new List<Bounds> ();
		
		foreach (Bounds rockBounds in rocksList) {
			listOfFire.Add (rockBounds);	
		}
		
		for (int i = 0; i < howMany; i++) {
			bool isIntersecting = false;
			Bounds rect = new Bounds ();
			Vector3 tempVec = new Vector3 ();
			// Getting a random scale - divided by two beacause extents are just half size
			tempVec.x = CTools.getFloatRandom ((CTools.FIRE_MIN_STRETCH), (CTools.FIRE_MAX_STRETCH)) / 2; 
			// Is the fire rotating ?
			bool isRotating = CTools._rnd.Next () % 2 == 0;
			// if the fire is rotating then it needs more space
			if (isRotating)
				tempVec.y = rect.extents.x;
			else
				tempVec.y = CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.FIRE_PREFAB_LOCATION).renderer.bounds.extents.y;
			// for collision purposes
			tempVec.z = 3;
			rect.extents = tempVec;
			// Getting x position - position local in RandomEvent field. Adding width/2 to keep fire on the background.
			tempVec.x = CTools.getFloatRandom ((float)((Math.Ceiling (- backgroundObject.renderer.bounds.extents.x) + rect.extents.x)), (float)(Math.Ceiling (backgroundObject.renderer.bounds.extents.x) / 2f - rect.extents.x));
			// Getting y position 
			tempVec.y = CTools.getFloatRandom (0, CTools.RANDOM_REGION);
			
			rect.center = tempVec;
			// Our fire is ready, now checking if it doesn't collide with any other in the field
			// if the list is empty then add the fire	
			// Check for collision
			foreach (Bounds actRect in listOfFire) {
				// if there's a collision check next rectangle
				if (rect.Intersects (actRect)) {
					isIntersecting = true;
					break;
				}
			}
			if (!isIntersecting) {	
				int rotationSpeed = 0;
				if (isRotating) {
					if (CTools.getRandomBool ())
						rotationSpeed = -CTools.FIRE_ROTATION_SPEED;
					else
						rotationSpeed = CTools.FIRE_ROTATION_SPEED;	
				}
				
				pRandomFireEvent.addFire (rect.center.x, rect.center.y, rect.extents.x, rotationSpeed);
				// adding offset so there's more place for the wagon
				rect.extents = new Vector3 (rect.extents.x + CTools.FIRE_X_OFFSET / 2, rect.extents.y + CTools.FIRE_Y_OFFSET / 2, 3);
				listOfFire.Add (rect);	
			}
		}	
	}
	
	// Adding maps to lists to get picked later
	private static void prepareListOfMaps ()
	{
		CRandomEventFactory._listOfMaps.Add (CTools.MAP_EVENT1_DATA);
		CRandomEventFactory._listOfMaps.Add (CTools.MAP_EVENT2_DATA);
		CRandomEventFactory._listOfMaps.Add (CTools.MAP_EVENT3_DATA);
		CRandomEventFactory._listOfCoinMaps.Add (CTools.MAP_COINS1_DATA);
		CRandomEventFactory._listOfCoinMaps.Add (CTools.MAP_COINS2_DATA);
		CRandomEventFactory._listOfCoinMaps.Add (CTools.MAP_COINS3_DATA);
		CRandomEventFactory._listOfCoinMaps.Add (CTools.MAP_COINS4_DATA);
		
	}
	
	
	}


