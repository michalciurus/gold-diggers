/*
* @file CWagonModel.cs
* The script attached to game's wagons that remembers it's movement 
* in a queue so a wagon following it can retrace it's movement.
*/
using UnityEngine;
using System;
using System.Collections.Generic;

public class CWagonModel : MonoBehaviour
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	
/*
* A struct that holds info about a saved moment of a wagon
* so a wagon following it can retrace it.
* 
*/
	private struct SMoment
	{
		public	Vector3 position;
		public Quaternion angle;
	
		public SMoment (Vector3 pPosVector, Quaternion pAngleVector)
		{
			position = pPosVector;
			angle = pAngleVector;
		}
	}
	private GameObject _wagonObject;
	private Queue<SMoment> _momentQueue;
	// If the wagon is followed it gets flagged so it remembers it's positions in a queue.
	private bool _followed;
	private CWagonModel _followedWagon;
	private ICollisionListener _collisionListener;
	private GameObject _thrusterParticleLeft;
	private GameObject _thrusterParticleRight;
	
//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	/*
	 * Initializing	
	 */
	
	private void Awake ()
	{
		this._followed = false;
		this._followedWagon = null;
		this._thrusterParticleLeft = null;
		this._thrusterParticleRight = null;
	
		this._wagonObject = this.gameObject;
		
		
		this._momentQueue = new Queue<SMoment> ();
		
	}
	
	private void Start ()
	{	
	}
	
	/*
	 * Pushes a moment into a stack
	 */
	private void pushMomentToStack ()
	{
		_momentQueue.Enqueue (new SMoment (this._wagonObject.transform.position, this._wagonObject.transform.rotation));
	}
	
	
	// sending collider messages to collision listener
	void OnTriggerEnter (Collider pCollision)
	{
		if (this._collisionListener != null) {
			if (pCollision.collider.gameObject.name == CTools.FIRE_OBJECT_NAME + "(Clone)") {
				
				this._collisionListener.fireCollision (pCollision.collider.gameObject);
			} else if (pCollision.collider.gameObject.name == CTools.ROCK_OBJECT_COLLIDER_NAME) {
				
				this._collisionListener.rockCollision (pCollision.collider.gameObject.transform.parent.gameObject);
			} else if (pCollision.collider.gameObject.name == CTools.COIN_OBJECT_NAME + "(Clone)") {
				
				this._collisionListener.coinCollision (pCollision.collider.gameObject);
			} else if (pCollision.collider.gameObject.name == CTools.MONSTER_OBJECT_NAME + "(Clone)") {
				
				this._collisionListener.monsterCollision (pCollision.collider.gameObject);
			} else if (pCollision.collider.gameObject.name == CTools.PLANE_POWER_OBJECT) {
				
				this._collisionListener.planeUpCollision (pCollision.collider.gameObject);
			} else if (pCollision.collider.gameObject.name == CTools.LIFE_POWER_OBJECT) {
				
				this._collisionListener.lifeUpCollision (pCollision.collider.gameObject);	
			}
		}
	}
	
	private SMoment dequeueMoment ()
	{
		return this._momentQueue.Dequeue ();	
	}
	
//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	/*
	 * gameObject getter
	 * 
	 * @param 
	 */
	public GameObject getWagonObject ()
	{
		return this.gameObject;
	}
	
	/*
	 * If followed it needs to put objects into a queue
	 */
	public void Update ()
	{
		if (this._followed) {
			this.pushMomentToStack ();	
		}
		// If it's following a wagon which has enough of moments stored
		if (this._followedWagon != null && this._followedWagon.getMomentQueueSize () >= CTools.WAGON_DELAY) {
			SMoment dequeuedMoment = this._followedWagon.dequeueMoment ();
			// retrace the moment
			this._wagonObject.transform.rotation = dequeuedMoment.angle;
			this._wagonObject.transform.position = 
				new Vector3 (dequeuedMoment.position.x,
					dequeuedMoment.position.y + CTools.WAGON_OFFSET_POSITION, 
					dequeuedMoment.position.z);
		}
	}
	
	/*
	 * Creates thrusters particle at given wagon
	 */
	public void createThrustersAtWagon ()
	{
		this._thrusterParticleLeft = MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.LPARTICLE_DATA)) as GameObject;
		this._thrusterParticleRight = MonoBehaviour.Instantiate (CGraphicManager.GameLoaderInstance.getPrefabFromCache (CTools.RPARTICLE_DATA)) as GameObject;
		
		this._thrusterParticleLeft.transform.parent = this._wagonObject.transform;
		this._thrusterParticleRight.transform.parent = this._wagonObject.transform;
		Vector3 mainWagonPosition = this._wagonObject.transform.position;
		this._thrusterParticleLeft.transform.position = new Vector3 (mainWagonPosition.x - this._wagonObject.renderer.bounds.extents.x / 2 - CTools.THRUSTERS_OFFSET, mainWagonPosition.y, mainWagonPosition.z);
		this._thrusterParticleRight.transform.position = new Vector3 (mainWagonPosition.x + this._wagonObject.renderer.bounds.extents.x / 2 + CTools.THRUSTERS_OFFSET, mainWagonPosition.y, mainWagonPosition.z);
	}
	
	/*
	 * --
	 * @param pWhichWagonTofollow sets a wagon that this wagon will follow
	 */
	public void setFollow (CWagonModel pWhichWagonTofollow)
	{
		this._followedWagon = pWhichWagonTofollow;
		this._followedWagon.setFollowed (true);
	}
	
	/*
	 * _followed setter
	 */
	public void setFollowed (bool isFollowed)
	{
		if ( this._followed == true && isFollowed == false)
			this._momentQueue.Clear();
		
		this._followed = isFollowed;
		
	}
	
	/*
	 * Get size of the queue
	 */
	public int getMomentQueueSize ()
	{
		return _momentQueue.Count;
	}
	
	public void setCollisionListener (ICollisionListener pCollisionListener)
	{
		this._collisionListener = pCollisionListener;
	}
	
}