﻿// @file CPauseButton
// script for button that helps to listen for events

using UnityEngine;
using System.Collections;

public class CPauseButton : MonoBehaviour {


	private IButtonClickListener _clickListener;
	
	void Awake()
	{
		this._clickListener = null;
		
	}
	
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick()
	{
	
		if ( this._clickListener != null)
		{
				Debug.Log (this.gameObject.name);
			this._clickListener.buttonClicked(this.gameObject);
		}
	
	}
	
	// Setting a click listener
	public void setClickListener(IButtonClickListener pListenerToClick)
	{
		this._clickListener = pListenerToClick;
	}
}
