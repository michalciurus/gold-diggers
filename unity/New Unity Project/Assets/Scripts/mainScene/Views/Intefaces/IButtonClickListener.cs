// @file IButtonCliclListener
// applied to buttons 

using System;
using UnityEngine;

public interface IButtonClickListener
{
	void buttonClicked (GameObject button);
	
}


