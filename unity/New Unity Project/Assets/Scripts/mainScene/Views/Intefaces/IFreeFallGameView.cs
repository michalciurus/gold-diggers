// @file IFreeFallGameView.cs
// Interface for view component

using System;

public interface IFreeFallGameView
{
	void updateView ();
	void updateLabels();
}


