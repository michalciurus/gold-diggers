// @file CGameplayView.cs
// View component for the game
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CGameplayView : MonoBehaviour, IFreeFallGameView, IButtonClickListener
{
	
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private CFreeFallGameplayModel _gameplayModel;
	private GameObject _backgroundObject;
	private GameObject _coinLabel;
	private GameObject _pausePanel;
	private GameObject _gamePanel;
	private GameObject _controlCircle;
	private CPauseButton _pauseButton;
	private CPauseButton _restartButton;
	private CPauseButton _resumeButton;
	
	private UILabel _lifeLabel;
	private UILabel _planeLabel;
	private UILabel _inviLabel;
	private UILabel _coinsGameLabel;
//------------------------------------------------------
// Public Methods
//------------------------------------------------------	
// constructor
	// @param pGameplayModel - model for data extraction

	
	public void Start()
	{
		this._gameplayModel = this.gameObject.GetComponent(typeof(CFreeFallGameplayModel)) as CFreeFallGameplayModel;
		this._pausePanel = GameObject.Find (CTools.PAUSE_PANEL);
		this._gamePanel = GameObject.Find (CTools.GAME_PANEL);
		this._backgroundObject = GameObject.Find (CTools.BACKGROUND_OBJECT_NAME);
		this._controlCircle = GameObject.Find (CTools.CONTROL_CIRCLE_NAME);
		
		_lifeLabel = GameObject.Find ("LifeNumberGame").GetComponent<UILabel>();
		_planeLabel = GameObject.Find ("PlaneNumberGame").GetComponent<UILabel>();
		_inviLabel = GameObject.Find ("InviNumberGame").GetComponent<UILabel>();

		this.setUI ();
	}
	
	public void init()
	{
		this.showGameMenu();
		this.updateLabels();
	}
	
	// interface member for buttons
	// @param pButton - button that was clicked
	public void buttonClicked (GameObject pButton)
	{
		if (pButton.name == this._pauseButton.name) {
			this.showPauseMenu ();
			this._gameplayModel.pauseGame ();
		} else if (pButton.name == this._resumeButton.name) {
			Debug.Log ("RESUME");
			this.showGameMenu ();
			this._gameplayModel.startGame ();
		} else if (pButton.name == this._restartButton.name) {
			this.showGameMenu ();
			this._gameplayModel.restartGame ();
		
		} 
	}	
	// Update is called once per frame
	public void updateView ()
	{
		if (this._coinLabel != null)
			(this._coinLabel.GetComponent (typeof(UILabel)) as UILabel).text = this._gameplayModel.getCoins ().ToString ();
	
		updateControlCircle(_gameplayModel.getCurrentTargetX());
		
		//if (this._backgroundObject != null)
		//Simulate movement of the world by changing texture offset
		if (this._backgroundObject != null)
			this._backgroundObject.renderer.material.SetTextureOffset ("_MainTex", new Vector2 (0, _backgroundObject.renderer.material.GetTextureOffset ("_MainTex").y - CTools.BACKGROUND_SPEED * Time.deltaTime));
	}
	
	// Setting UI members
	public void setUI ()
	{
		// setting click listeners to buttons
		this._pauseButton = GameObject.Find (CTools.PAUSE_OBJECT).GetComponent (typeof(CPauseButton)) as CPauseButton;
		this._pauseButton.setClickListener (this);
		this._resumeButton = GameObject.Find (CTools.RESUME_OBJECT).GetComponent (typeof(CPauseButton)) as CPauseButton;
		this._resumeButton.setClickListener (this);
		this._restartButton = GameObject.Find (CTools.RESTART_OBJECT).GetComponent (typeof(CPauseButton)) as CPauseButton;
		this._restartButton.setClickListener (this);

		this._coinLabel = GameObject.Find (CTools.COIN_LABEL);
		
		// set pause panel inactive
		NGUITools.SetActive (this._pausePanel, false);
	}
	
	public void updateLabels()
	{
		_lifeLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerLives;
		_planeLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerPlanes;
		_inviLabel.text = XMLGameData.XMLGameDataInstance._numberOfPowerInvi;
	}
//------------------------------------------------------
// Private Methods
//------------------------------------------------------

	// Swapping panels
	public void showPauseMenu()
	{
		NGUITools.SetActive (this._gamePanel, false);
		NGUITools.SetActive (this._pausePanel, true);
	}
	
	public void finishGame()
	{
		this._resumeButton.gameObject.SetActive(false);
		//GameObject.Find("distance").GetComponent<UILabel>().text;
		this.showPauseMenu();
		GameObject.Find ("coins").GetComponent<UILabel>().text = "Coins Gathered: " + _gameplayModel.getCoins().ToString();
		
	}
	
	// Swapping panels
	private void showGameMenu ()
	{
		NGUITools.SetActive (this._gamePanel, true);
		NGUITools.SetActive (this._pausePanel, false);
	}

	//Simulate movement of the world by changing texture offset
	private void moveTextureOffset()
	{
		Rect actRect = (_backgroundObject.GetComponent<UITexture>()).uvRect;
		actRect.y -= CTools.BACKGROUND_SPEED;
		(_backgroundObject.GetComponent<UITexture>()).uvRect = actRect;
	}
	
	// updates the control circle
	// @param - x to which the circle is to be set
	private void updateControlCircle(float x)
	{
		if ( ! (this._controlCircle.transform.position.x < x + 0.01 && this._controlCircle.transform.position.x > x - 0.01))
		{
		Vector3 act = this._controlCircle.transform.position;
		this._controlCircle.transform.position = new Vector3( x, act.y, act.z);
		this._controlCircle.transform.Rotate(new Vector3(0,0, _controlCircle.transform.eulerAngles.z + 0.001f));
		}
	}
	
	private bool mouseIdle()
	{
		return !CGameplayController.touchDown;
	}
	
}