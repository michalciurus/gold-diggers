﻿/*
* @file CGameplayController.cs
* Implementing MVC pattern. This class is the main controller in the pattern. It reads input data and responds by changing model.
*/

#define WINDOWS
using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class CGameplayController : MonoBehaviour
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	private CFreeFallGameplayModel _model;
	private CGameplayView _view;
	private bool _gameStarted;
	public static bool touchDown = false;
//------------------------------------------------------
// Private Methods
//------------------------------------------------------
	
	private void Awake()
	{
		this.loadAssets();
		this._gameStarted = false;
		XMLGameData k = XMLGameData.XMLGameDataInstance;
		k.Load ();
		CAchievmentsManager.managerInstance.updateXML();
	}
	
	
	private void Start ()
	{
		Application.targetFrameRate = CTools.FRAMERATE;
		this._model = this.GetComponent(typeof(CFreeFallGameplayModel)) as CFreeFallGameplayModel;
		this._view = this.GetComponent(typeof(CGameplayView)) as CGameplayView;
	}
	
	private void Update ()
	{
		if ( !this._gameStarted )
			return;
		
		this._model.updateModel ();
		this._view.updateView ();
	}
	
	public void init()
	{
		this._gameStarted = true;
	}
	
	private void disable()
	{
		
	}
	
	public void tryUseLife()
	{
		int numberOfLifes = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfPowerLives);
		if ( numberOfLifes > 0)
		{
			numberOfLifes--;
			_model.addLife();
			XMLGameData.XMLGameDataInstance._numberOfPowerLives = numberOfLifes.ToString();
			XMLGameData.XMLGameDataInstance.Save();	
		}
		_view.updateLabels();
	}
	
	public void tryUsePlane()
	{
		int numberOfPlanes = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfPowerPlanes);
		if ( numberOfPlanes > 0)
		{
			numberOfPlanes--;
			_model.addPlane();
			XMLGameData.XMLGameDataInstance._numberOfPowerPlanes = numberOfPlanes.ToString();
			XMLGameData.XMLGameDataInstance.Save();	
		}
		_view.updateLabels();
	}
	
	public void tryUseInvi()
	{
		int numberOfInvi = Convert.ToInt32(XMLGameData.XMLGameDataInstance._numberOfPowerInvi);
		if ( numberOfInvi > 0 && _model.getTrainState() == CTrain.trainState.NORMAL)
		{
			numberOfInvi--;
			_model.addInvi();
			XMLGameData.XMLGameDataInstance._numberOfPowerInvi = numberOfInvi.ToString();
			XMLGameData.XMLGameDataInstance.Save();	
		}
		_view.updateLabels();
	}
	
	// Dealing with input in LateUpdate so the game knows if any UI has been pressed
	private void LateUpdate ()
	{
		if ( !this._gameStarted )
			return;
		
		#if WINDOWS
		if (Input.GetMouseButton (0)) {	
			if (UICamera.hoveredObject != null && UICamera.hoveredObject.gameObject.tag == "BACKGROUND")
			{
				CGameplayController.touchDown = true;
				this._model.inputOnPosition (Camera.main.ScreenToWorldPoint (Input.mousePosition));
			}
		}
		else if ( Input.GetMouseButtonUp(0))
		{
			CGameplayController.touchDown = false;
		}
		
#elif ANDROID
		// Android input not implemented yet
		if ( Input.touches[0].phase == TouchPhase.Moved )
		{			
			CGameplayController.touchDown = true;
			_model.inputOnPosition(Camera.main.ScreenToWorldPoint (Input.touches[0].position));
		}
		else if ( Input.touches[0].phase == TouchPhase.Ended)
		{
				CGameplayController.touchDown = false;
		}
#endif
	}
	
	private void loadAssets()
	{
		CGraphicManager.GameLoaderInstance.loadAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.GAME_ATLAS_TEXTURE_NAME);
		
			CGraphicManager.GameLoaderInstance.cacheTexture(CTools.BACKGROUND_TEXTURE_LOCATION);
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.BUTTON_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME,CTools.FIRE_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.COIN_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.ROCK1_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.WAGON_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.WHEEL_TEXTURE );
			CGraphicManager.GameLoaderInstance.cacheTextureFromAtlas(CTools.GAME_ATLAS_JSON_NAME, CTools.MONSTER_TEXTURE );
		
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.COIN_PREFAB_LOCATION  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.FIRE_PREFAB_LOCATION  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.ROCK1_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.LPARTICLE_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.RPARTICLE_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.WAGON_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.EMPTY_EVENT_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.FIRE_EVENT_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.MONSTER_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.COINS_EVENT_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.MONSTER_EVENT_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.MAP_EVENT1_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.MAP_EVENT2_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.MAP_EVENT3_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	MAP_COINS1_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	MAP_COINS2_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	MAP_COINS3_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	MAP_COINS4_DATA  );
			CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	PLANE_DATA );
		CGraphicManager.GameLoaderInstance.cachePrefab(CTools.	MISSILE_DATA );
		CGraphicManager.GameLoaderInstance.cachePrefab(CTools.ACHIEVMENT_PREFAB_LOCATION );
	}
	
	
}
