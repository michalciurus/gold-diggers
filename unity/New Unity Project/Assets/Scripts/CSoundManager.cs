﻿// @file CSoundManager.cs
// loads audio sources and caches them

using UnityEngine;
using System.Collections;

public class CSoundManager : MonoBehaviour,ISoundManager
{
//------------------------------------------------------
// Private Variables
//------------------------------------------------------
	AudioSource _monster;
	AudioSource _damage;
	AudioSource _shoot;
	AudioSource _coin;
	public static ISoundManager soundManagerInstance;

//------------------------------------------------------
// Private Methods
//------------------------------------------------------

	// Getting the sounds to call them later
	void Start ()
	{
		Component[] sources = GetComponents (typeof(AudioSource));
		
		foreach (Component source in sources) {
		
			if ((source as AudioSource).clip.name == CTools.MONSTER_SOUND) {
				this._monster = (source as AudioSource);
			} else if ((source as AudioSource).clip.name == CTools.SHOOT_SOUND) {
				this._shoot = (source as AudioSource);	
			} else if ((source as AudioSource).clip.name == CTools.DAMAGE_SOUND) {
				this._damage = (source as AudioSource);
			} else if ((source as AudioSource).clip.name == CTools.COIN_SOUND) {
				this._coin = (source as AudioSource);
			}
		}
		
		CSoundManager.soundManagerInstance = this;
		
	
	}

//------------------------------------------------------
// Public Methods
//------------------------------------------------------
	
	public void playMonsterSound ()
	{
		this._monster.Play ();
	}
	
	public void playShootSound ()
	{
		this._shoot.Play ();
	}
	
	public void playDamageSound ()
	{
		this._damage.Play ();
	}
	
	public void playCoinSound ()
	{
		this._coin.Play ();
	}
	
	void Update ()
	{

	}
	
	public void muteSounds()
	{
		audio.mute = !audio.mute;
	}
}
